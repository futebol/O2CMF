# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from __future__ import absolute_import

from gcf.geni.am.resource import Resource


class ComputeNode(Resource):
    """
    Models a OpenStack compute node.
    It is only used in advertisement RSPEC, but we actually offer VMs.
    """

    def __init__(self, rid):
        super(ComputeNode, self).__init__(rid, "node")
        self.exclusive = False
