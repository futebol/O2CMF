# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------


class User:
    """
    Represents a user of the federation. It was created just to store the data needed to enable the SSH access.
    """

    def __init__(self, name, user_urn):
        self.id = None
        self.name = name
        self.urn = user_urn
