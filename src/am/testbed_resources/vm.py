# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from __future__ import absolute_import

from gcf.geni.am.resource import Resource


class VirtualMachine(Resource):
    """
    Class that models the VM resource.
    """

    def __init__(self):
        super(VirtualMachine, self).__init__(None, "vm")
        self.image = None
        self.flavor = None
        self.interfaces = []
        self.login = None  # the username
        self.access = None  # the control plane IP address

    def merge(self, parent_resource):
        """
        Merges the VM instance with an instance of its parent class (Resource).
        :return: None
        """
        self.available = parent_resource.available
        self.external_id = parent_resource.external_id
        self.state = parent_resource.state


class NetworkInterface:
    # Class that models the VM's network interfaces.

    def __init__(self, name, ip_type, addr, mask):
        self.name = name
        self.type = ip_type
        self.address = addr
        self.netmask = mask
        self.bridge = None  # must be in the format: bridge0:eth0->link0
