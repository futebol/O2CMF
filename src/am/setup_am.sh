#!/bin/bash

# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo,
# Diego Giacomelli Cardoso, Rafael Silva Guimaraes
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

AM_PATH=$(pwd)

echo "====================== Installing OpenStack dependencies ======================"
apt update
apt install python-dev python-pip -y
pip install shade requests netaddr

echo "========================= Installing DB dependencies =========================="
pip install SQLAlchemy
apt install python-mysqldb -y

echo "========================= Installing GCF dependencies ========================="
apt install python-m2crypto python-dateutil python-openssl libxmlsec1 xmlsec1 libxmlsec1-openssl libxmlsec1-dev -y

echo "==================== Making our packages visible to Python ===================="
echo "export PYTHONPATH=\"${PYTHONPATH}:${AM_PATH}/:${AM_PATH}/gcf-2_10/src/\"" >> ~/.bashrc
echo "export AM_PATH=\"${AM_PATH}/\"" >> ~/.bashrc

exec bash
