# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo,
# Diego Giacomelli Cardoso
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from __future__ import absolute_import

import dateutil.parser
import json
import requests

from database.persistence import Persistence
from gcf.geni.am.am3 import *
from gcf.geni.am.api_error_exception import ApiErrorException
from gcf.geni.auth.base_authorizer import *
from gcf.geni.config import *
from gcf.geni.util import urn_util as urn
from gcf.geni.util.tz_util import tzd
from gcf.geni.util.urn_util import publicid_to_urn
from testbed_resources.compute_node import ComputeNode
from testbed_resources.user import User
from testbed_resources.image import Image
from testbed_resources.vm import *
from drivers.openstack_driver import OpenstackOperation


class CloudAggregateManager(ReferenceAggregateManager):
    """
    Class that implements the SFA API in order expose VMs to the federation.
    """

    # noinspection PyMissingConstructor,PyUnusedLocal
    def __init__(self, root_cert, urn_authority, url, **kwargs):
        self._urn_authority = urn_authority
        self._url = url
        self._cred_verifier = geni.CredentialVerifier(root_cert)
        self._api_version = 3
        self._am_type = "gcf"
        self.__persistence = Persistence()
        self._slices = self.__persistence.retrieve_slices()
        self._agg = Aggregate()
        self._my_urn = publicid_to_urn("IDN %s %s %s" % (self._urn_authority, 'authority', 'am'))
        self.max_lease = datetime.timedelta(minutes=REFAM_MAXLEASE_MINUTES)
        self.max_alloc = datetime.timedelta(seconds=ALLOCATE_EXPIRATION_SECONDS)
        self.__cloud = OpenstackOperation()
        file_path = os.environ["AM_PATH"] + "config.ini"
        config = read_config(file_path)
        url_pattern = "http://%s:%s/add/user"
        self.__ssh_url = url_pattern % (config["ssh"]["ip"], config["ssh"]["port"])
        self.__proxy_url = config["ssh"]["dns_name"]
        self.__ssh_token = config["ssh"]["token"]
        self.logger = logging.getLogger('gcf.am3')
        self.logger.info("Running %s AM v%d code version %s | FUTEBOL UFES Cloud AM", self._am_type, self._api_version,
                         GCF_VERSION)

    def expire_slivers(self):
        """
        Look for expired slivers and clean them up.
        It is called at the beginning of all SFA methods.
        :return: None
        """
        self.logger.info('expire_slivers')
        expired_info = self.__persistence.delete_expired_allocations()

        for pair in expired_info:
            # pair = (username, project_name)
            self.logger.info('expire_slivers: releasing resources given to %s in the project %s.', pair[0], pair[1])
            try:
                # deletes user's keys on proxy
                data = {
                    "token": self.__ssh_token,
                    "DeleteUserSSH": {
                        "username": pair[0]
                    }
                }
                resp = requests.post(url=self.__ssh_url, json=data)
                json.loads(resp.text)
            except:
                self.logger.warning('expire_slivers: failed to remove old users.')

            try:
                # release resources in the cloud.
                self.__cloud.purge_project(pair[0], pair[1])
            except:
                self.logger.warning('expire_slivers: failed to remove resources from openstack.')

        self._slices = self.__persistence.retrieve_slices()

    def ListResources(self, credentials, options):
        """
        Look for available resources on the testbed and and exposes this info for the user.
        :param credentials: the user credential (issued by the federation).
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a data structure containing the advertisement RSPEC.
        """
        self.expire_slivers()
        self.logger.info('ListResources(%r)' % options)

        # Parameter validation
        privileges = ()
        self.getVerifiedCredentials(None, credentials, options, privileges)
        if 'geni_rspec_version' not in options:
            self.logger.error('No geni_rspec_version supplied to ListResources.')
            return self.errorResult(AM_API.BAD_ARGS, 'Bad Arguments: option geni_rspec_version was not supplied.')
        if 'type' not in options['geni_rspec_version']:
            self.logger.error('ListResources: geni_rspec_version does not contain a type field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a type field.')
        if 'version' not in options['geni_rspec_version']:
            self.logger.error('ListResources: geni_rspec_version does not contain a version field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a version field.')

        rspec_type = options['geni_rspec_version']['type']
        if isinstance(rspec_type, str):
            rspec_type = rspec_type.lower().strip()
        rspec_version = options['geni_rspec_version']['version']
        if rspec_type != 'geni':
            self.logger.error('ListResources: Unknown RSpec type %s requested', rspec_type)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec type %s is not a valid option.' % rspec_type)
        if rspec_version != '3':
            self.logger.error('ListResources: Unknown RSpec version %s requested', rspec_version)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec version %s is not a valid option.' % rspec_version)
        self.logger.info("ListResources requested RSpec %s (%s)", rspec_type, rspec_version)

        if 'geni_slice_urn' in options:
            self.logger.error('ListResources: geni_slice_urn is no longer a supported option.')
            msg = 'Bad Arguments:'
            msg += 'option geni_slice_urn is no longer a supported option.'
            msg += ' Use "Describe" instead.'
            return self.errorResult(AM_API.BAD_ARGS, msg)

        # Creating the advertisement rspec
        resource_xml = self.advert_resource()
        result = self.advert_header() + resource_xml + self.advert_footer()

        if 'geni_compressed' in options and options['geni_compressed']:
            try:
                result = base64.b64encode(zlib.compress(result))
            except Exception, exc:
                self.logger.error("Error compressing and encoding resource list: %s", traceback.format_exc())
                raise Exception("Server error compressing resource list", exc)

        return self.successResult(result)

    def advert_header(self):
        """
        Composes the header of the advertisement RSPEC.
        :return: string containing the header.
        """
        schema_locs = ["http://www.geni.net/resources/rspec/3",
                       "http://www.geni.net/resources/rspec/3/ad.xsd",
                       "http://www.geni.net/resources/rspec/ext/opstate/1",
                       "http://www.geni.net/resources/rspec/ext/opstate/1/ad.xsd"]
        header = '''<?xml version="1.0" encoding="UTF-8"?>
<rspec xmlns="http://www.geni.net/resources/rspec/3"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="%s"
       type="advertisement">
'''
        return header % (' '.join(schema_locs))

    def advert_resource(self, resources=None):
        """
        Composes the tags that describe managed resources.
        It is part of the advertisement RSPEC.
        :param resources: a legacy parameter. Actually, it is not used here.
        :return: string containing the tags.
        """
        node = ComputeNode("compute_node")
        adv_body = '\t<node component_manager_id="%s" component_name="%s" component_id="%s" exclusive="false">\n' \
                   % (self._my_urn, node.id, node.urn(self._urn_authority))
        flavor_and_image = self.__cloud.list_image_per_flavor()
        for flavor in flavor_and_image.keys():
            adv_body += '\t\t<sliver_type name="vm-%s">\n' % flavor
            for image in flavor_and_image[flavor]:
                img = Image(image)
                adv_body += '\t\t\t<disk_image name="%s"/>\n' % img.urn(self._urn_authority)
            adv_body += '\t\t</sliver_type>\n'
        adv_body += '\t\t<available now="true"/>\n\t</node>\n'

        return adv_body

    def Allocate(self, slice_urn, credentials, rspec, options):
        """
        Reserve resources in the testbed for a user.
        :param slice_urn: the URN of the slice created by the federation.
        :param credentials: the user credential (issued by the federation).
        :param rspec: specifies the resources to be allocated.
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a data structure containing the manifest RSPEC (listing the resources actually reserved).
        """
        self.expire_slivers()
        self.logger.info('Allocate(%r)' % slice_urn)

        privileges = (ALLOCATE_PRIV,)

        # Authorization and User data
        creds = self.getVerifiedCredentials(slice_urn, credentials, options, privileges)
        user_urn = gid.GID(string=options['geni_true_caller_cert']).get_urn()
        username = user_urn.split("+")[-1]
        user = User(username, user_urn)
        self.logger.info('User URN: %s' % user_urn)

        # Parsing the rspec
        # noinspection PyUnusedLocal
        rspec_dom = None
        try:
            rspec_dom = minidom.parseString(rspec)
        except Exception, exc:
            self.logger.error("Cannot create sliver %s. Exception parsing rspec: %s" % (slice_urn, exc))
            return self.errorResult(AM_API.BAD_ARGS, 'Bad Args: RSpec is unparseable')

        # Mapping the RSpec elements to objects
        # NOTE: This only handles unbound nodes. Any attempt by the client to specify a node is ignored.
        cloud_request = {"name": slice_urn, "user": user, "resources": {}}
        # NOTE: the value of cloud_request["resources"] should be always a dictionary using the resource's external id
        # as key and the resource object as value.

        nodes = rspec_dom.documentElement.getElementsByTagName('node')
        bridges = []
        for elem in nodes:
            sliver_elem = elem.getElementsByTagName('sliver_type')[0]
            sliver_type_advert = sliver_elem.getAttribute('name')

            if sliver_type_advert.find("vm") >= 0:
                vm = VirtualMachine()
                vm.available = False

                # sets the name of the vm
                try:
                    vm.external_id = elem.getAttribute('client_id')
                except:
                    raise Exception('Bad Args: Node missing client_id')

                # sets the flavor of the vm
                flavor = sliver_type_advert[3:]
                vm.flavor = flavor

                # sets the vm image
                try:
                    image_tag = sliver_elem.getElementsByTagName('disk_image')[0]
                    vm.image = image_tag.getAttribute('name').split("+")[-1]
                except:
                    flavor_image = self.__cloud.list_image_per_flavor()
                    vm.image = flavor_image[vm.flavor][0]

                # sets the vm's network interfaces
                ifaces = elem.getElementsByTagName('interface')

                if (len(nodes) == 1) and (len(ifaces) == 0):
                    # this case may be valid for experiments using only one vm (or NFV with the orchestrator)
                    iface = NetworkInterface("eth0", "ipv4", "10.255.255.254", "255.255.255.0")
                    iface.bridge = "bridge0:eth0->link0"
                    vm.interfaces.append(iface)
                else:
                    try:
                        iface_elem = ifaces[0]
                        ip_elem = iface_elem.getElementsByTagName('ip')[0]
                        ip_type = ip_elem.getAttribute('type')
                        if ip_type == "":
                            ip_type = "ipv4"
                        ip_mask = ip_elem.getAttribute('netmask')
                        if ip_mask == "":
                            ip_mask = "255.255.255.0"
                        iface = NetworkInterface(iface_elem.getAttribute('client_id').split(":")[1], ip_type,
                                                 ip_elem.getAttribute('address'), ip_mask)
                        iface.bridge = "bridge0:eth0->link0"
                        vm.interfaces.append(iface)
                    except:
                        raise Exception('Bad Args: Node network interface missing parameters')

                cloud_request["resources"][vm.external_id] = vm

                log_ips = [net.address for net in vm.interfaces]
                self.logger.info('VM(flavor: %s, image: %s, external_id: %s, IPs: %r)', vm.flavor, vm.image,
                                 vm.external_id, log_ips)

            elif sliver_type_advert == "bridge":
                client_id = elem.getAttribute('client_id')
                bridges.append(client_id)

            else:
                raise Exception('Bad Args: RSpec contains illegal NODE elements')

        # Verify the availability of resources
        available = self.__cloud.verify_resource_availability(cloud_request)
        if not available:
            self.logger.error('Too big: There are not enough resources')
            return self.errorResult(AM_API.TOO_BIG, 'Too Big: insufficient resources to fulfill request')

        # validates if every link connects a node to a bridge
        for elem in rspec_dom.documentElement.getElementsByTagName('link'):
            child_elem = elem.getElementsByTagName('interface_ref')
            node = child_elem[0].getAttribute('client_id').split(":")[0]
            bridge = child_elem[1].getAttribute('client_id')
            if bridge.split(":")[0] in bridges:
                try:
                    cloud_request["resources"][node].interfaces[0].bridge = bridge + "->" + \
                                                                            elem.getAttribute('client_id')
                except:
                    raise Exception('Bad Args: RSpec contains illegal net iface')
            else:
                raise Exception('Bad Args: RSpec contains illegal arguments')

        # Determine end time as min of the slice and the requested time (if any)
        end_time = self.min_expire(creds, requested=('geni_end_time' in options and options['geni_end_time']))

        # Determine the start time as bounded by slice expiration and 'now'
        now = datetime.datetime.utcnow()
        start_time = now
        if 'geni_start_time' in options:
            # Need to parse this into datetime
            start_time_raw = options['geni_start_time']
            start_time = self._naiveUTC(dateutil.parser.parse(start_time_raw))
        start_time = max(now, start_time)
        if start_time > self.min_expire(creds):
            return self.errorResult(AM_API.BAD_ARGS, "Can't request start time on sliver after slice expiration")

        # Determine max expiration time from credentials
        expiration = self.min_expire(creds, self.max_alloc, ('geni_end_time' in options and options['geni_end_time']))

        # If we're allocating something for future, give a window from start time in which to reserve
        if start_time > now:
            expiration = min(start_time + self.max_alloc, self.min_expire(creds))

        # If slice exists, check accept only if no existing sliver overlaps
        # with requested start/end time. If slice doesn't exist, create it.
        if slice_urn in self._slices:
            new_slice = self._slices[slice_urn]
            # Check if any current slivers overlap with requested start/end
            one_slice_overlaps = False
            for sliver in new_slice.slivers():
                if sliver.startTime() < end_time and \
                                sliver.endTime() > start_time:
                    one_slice_overlaps = True
                    break

            if one_slice_overlaps:
                template = "Slice %s already has slivers at requested time"
                self.logger.error(template % slice_urn)
                return self.errorResult(AM_API.ALREADY_EXISTS, template % slice_urn)
        else:
            new_slice = Slice(slice_urn)

        self.logger.info('slice duration: expiration: %s , start: %s , end: %s ', expiration, start_time, end_time)

        # fills resource's attributes with default values
        for resource in cloud_request["resources"].values():
            sliver = new_slice.add_resource(resource)
            sliver.setExpiration(expiration)
            sliver.setStartTime(start_time)
            sliver.setEndTime(end_time)
            sliver.setAllocationState(STATE_GENI_ALLOCATED)
        self._slices[slice_urn] = new_slice

        # Registering the resource allocation
        self.__persistence.save_allocation(user, new_slice)
        self.__persistence.save_future_cloud_request(cloud_request)

        # Log the allocation
        self.logger.info("Allocated new slice %s" % slice_urn)

        # Generating the manifest RSPEC
        manifest = self.manifest_header() + self.manifest_slice_allocate(slice_urn) + self.manifest_footer()
        result = dict(geni_rspec=manifest, geni_slivers=[s.status() for s in new_slice.slivers()])

        return self.successResult(result)

    def manifest_slice_allocate(self, slice_urn):
        """
        Composes the tags that describe reserved resources.
        It is part of the manifest RSPEC (issued from allocate method).
        :param slice_urn: the URN of the slice created by the federation.
        :return: a string containing the description of the reserved resources (without SSH info).
        """
        resources = ''
        bridges = ''
        links = ''

        bridges_set = set()
        bridges_list = list()
        my_slice = self.__persistence.retrieve_slice(slice_urn)

        for some_sliver in my_slice.slivers():
            resource = some_sliver.resource()

            if resource.type == "vm":
                # Describing the resource (a node)
                resources += '\t<node client_id="%s" component_manager_id="%s" sliver_id="%s" exclusive="false">\n' \
                             % (resource.external_id, self._my_urn, some_sliver.urn())

                resources += '\t\t<sliver_type name="vm-%s">\n' % resource.flavor
                img = Image(resource.image)
                resources += '\t\t\t<disk_image name="%s"/>\n' % img.urn(self._urn_authority)
                resources += '\t\t</sliver_type>\n'

                for iface in resource.interfaces:
                    resources += '\t\t<interface client_id="%s:%s">\n\t\t\t<ip address="%s" netmask="%s" ' \
                                 'type="%s"/>\n\t\t</interface>' % (resource.external_id, iface.name, iface.address,
                                                                    iface.netmask, iface.type)

                    # Describing the connectivity (links)
                    br_parse = iface.bridge.split("->")
                    links += '\t<link client_id="%s">\n\t\t<component_manager name="%s"/>\n' % \
                             (br_parse[1], self._my_urn)
                    # the origin is always a vm
                    origin = resource.external_id + ":" + iface.name
                    links += '\t\t<interface_ref client_id="%s"/>\n' % origin

                    # the destination is always a bridge
                    destination = br_parse[0]
                    links += '\t\t<interface_ref client_id="%s"/>\n' % destination
                    links += '\t\t<link_type name="lan"/>\t</link>\n'

                    # acquiring info to be used in the description of bridges elements
                    bridges_set.add(destination.split(":")[0])
                    bridges_list.append(destination)

                resources += '\t</node>\n'

        for bridge in bridges_set:
            # Describing the resource (a bridge)
            bridges += '\t<node client_id="%s" component_manager_id="%s" exclusive="false">\n\t\t<sliver_type ' \
                       'name="bridge">\n' % (bridge, self._my_urn)

            br_ifaces = [iface for iface in bridges_list if bridge in iface]
            for iface in br_ifaces:
                bridges += '\t\t\t<interface client_id="%s"/>\n' % iface

            bridges += '\t\t</sliver_type>\n\t</node>\n'

        return resources + bridges + links

    def decode_urns(self, urns, **kwargs):
        """
        Deduce a slice based on the URN and maps slices to slivers.
        :param urns: a list containing the slice and the slivers URNs.
        :param kwargs: a dictionary containing credentials and options.
        :return: a slice and a list of slivers.
        """
        self.logger.info('decode_urns(%r)' % urns)
        slices_urn = []
        slivers_urn = []
        # noinspection PyUnusedLocal
        my_slice = None
        slivers = list()

        # Separate urns by type
        for some_urn in urns:
            # confusing definition (beware of bugs)!!!
            current = urn.URN(urn=some_urn)
            urn_type = current.getType()
            if urn_type == 'slice':
                slices_urn.append(some_urn)
            elif urn_type == 'sliver':
                slivers_urn.append(some_urn)
            else:
                raise Exception("Bad URN type '%s'" % urn_type)

        # Verify that everything is part of the same slice
        if len(slices_urn) > 1:
            raise Exception('Objects specify multiple slices')
        else:
            try:
                # Retrieve the objects
                if slices_urn[0] in self._slices:
                    my_slice = self._slices[slices_urn[0]]
                else:
                    my_slice = self.__persistence.retrieve_slice(slices_urn[0])
                    self._slices[slices_urn[0]] = my_slice
                    if my_slice is None:
                        raise ApiErrorException(AM_API.SEARCH_FAILED, 'Unknown slice "%s"' % (slices_urn[0]))

                if my_slice.isShutdown():
                    msg = 'Refused: slice %s is shut down.' % my_slice.urn
                    raise ApiErrorException(AM_API.REFUSED, msg)

                slivers.extend(my_slice.slivers())
                return my_slice, slivers
            except:
                raise Exception('Bad Args: missing URL')

    def Provision(self, urns, credentials, options):
        """
        Instantiate the physical resources based on the allocation registered.
        :param urns: a list containing the slice and the slivers URNs.
        :param credentials: the user credential (issued by the federation).
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a data structure containing the manifest RSPEC (listing the resources actually instantiated).
        """
        self.expire_slivers()
        self.logger.info('Provision(%r)' % urns)

        # Retrieve the allocation registries
        the_slice, slivers = self.decode_urns(urns)

        # Authorization
        privileges = (PROVISION_PRIV,)
        creds = self.getVerifiedCredentials(the_slice.urn, credentials, options, privileges)

        # Parameter validation
        if 'geni_rspec_version' not in options:
            # This is a required option, so error out with bad arguments.
            self.logger.error('No geni_rspec_version supplied to Provision.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version was not supplied.')
        if 'type' not in options['geni_rspec_version']:
            self.logger.error('Provision: geni_rspec_version does not contain a type field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a type field.')
        if 'version' not in options['geni_rspec_version']:
            self.logger.error('Provision: geni_rspec_version does not contain a version field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a version field.')

        # Look to see what RSpec version the client requested
        # Error-check that the input value is supported.
        rspec_type = options['geni_rspec_version']['type']
        if isinstance(rspec_type, str):
            rspec_type = rspec_type.lower().strip()
        rspec_version = options['geni_rspec_version']['version']
        if rspec_type != 'geni':
            self.logger.error('Provision: Unknown RSpec type %s requested', rspec_type)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec type %s is not a valid option.' % rspec_type)
        if rspec_version != '3':
            self.logger.error('Provision: Unknown RSpec version %s requested', rspec_version)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec version %s is not a valid option.' % rspec_version)
        self.logger.info("Provision requested RSpec %s (%s)", rspec_type, rspec_version)

        # Only provision slivers that are in the scheduled time frame
        now = datetime.datetime.utcnow()
        provisionable_slivers = \
            [sliver for sliver in slivers if sliver.startTime() <= now <= sliver.endTime()]
        slivers = provisionable_slivers

        if len(slivers) == 0:
            return self.errorResult(AM_API.UNAVAILABLE, "No slivers available to provision at this time")

        max_expiration = self.min_expire(creds, self.max_lease,
                                         ('geni_end_time' in options and options['geni_end_time']))

        # Instantiating the resources
        request = self.__persistence.retrieve_cloud_request(urns[0])
        keys = options['geni_users'][0]['keys']
        reply = self.__cloud.setup_project(request, keys)  # returns a dict (vm name (key) and IP for ssh (value))

        self.logger.info('user: %s', request["user"].name)

        for sliver in slivers:
            # updates sliver's attributes
            expiration = min(sliver.endTime(), max_expiration)
            sliver.setEndTime(expiration)
            sliver.setExpiration(expiration)
            sliver.setAllocationState(STATE_GENI_PROVISIONED)
            sliver.setOperationalState(OPSTATE_GENI_NOT_READY)
            if sliver.resource().type == "vm":
                sliver.resource().login = request["user"].name
                sliver.resource().access = reply[sliver.resource().external_id]
                self.logger.info('VM: external_id: %s, floating IP: %s', sliver.resource().external_id,
                                 reply[sliver.resource().external_id])

        # Registering user in the SSH proxy
        data = {
            "token": self.__ssh_token,
            "CreateUserSSH": {
                "username": request["user"].name,
                "ssh-key": keys
            }
        }
        resp = requests.post(url=self.__ssh_url, json=data)
        x = json.loads(resp.text)
        self.logger.info('%r', x)

        # Store the status change
        self.__persistence.update_allocation(slivers)
        self.__persistence.delete_cloud_request(urns[0])

        # Create the manifest RSPEC
        result = dict(geni_rspec=self.manifest_rspec(the_slice.urn),
                      geni_slivers=[s.status() for s in slivers])
        return self.successResult(result)

    def manifest_slice(self, slice_urn):
        """
        Composes the tags that describe reserved resources.
        It is part of the manifest RSPEC.
        :param slice_urn: the URN of the slice created by the federation.
        :return: a string containing the description of the reserved resources (with SSH info).
        """
        resources = ''
        bridges = ''
        links = ''

        bridges_set = set()
        bridges_list = list()
        my_slice = self.__persistence.retrieve_slice(slice_urn)

        for some_sliver in my_slice.slivers():
            resource = some_sliver.resource()

            if resource.type == "vm":
                # Describing the resource (a node)
                resources += '\t<node client_id="%s" component_manager_id="%s" sliver_id="%s" exclusive="false">\n' \
                             % (resource.external_id, self._my_urn, some_sliver.urn())

                resources += '\t\t<sliver_type name="vm-%s">\n' % resource.flavor
                img = Image(resource.image)
                resources += '\t\t\t<disk_image name="%s"/>\n' % img.urn(self._urn_authority)
                resources += '\t\t</sliver_type>\n'

                for iface in resource.interfaces:
                    resources += '\t\t<interface client_id="%s:%s">\n\t\t\t<ip address="%s" netmask="%s" ' \
                                 'type="%s"/>\n\t\t</interface>' % (resource.external_id, iface.name, iface.address,
                                                                    iface.netmask, iface.type)

                    # SSH access data
                    resources += '\t\t<services xmlns:proxy="http://jfed.iminds.be/proxy/1.0">\n\t\t\t<proxy:proxy ' \
                                 'proxy="%s@%s:22" for="%s@%s:22"/>\n\t\t\t\t<login authentication="ssh-keys" ' \
                                 'hostname="%s" port="22" username="%s"/>\n\t\t\t\t<login authentication="ssh-keys" ' \
                                 'hostname="%s" port="22" username="%s"/>\n\t\t</services>\n' % \
                                 (resource.login, self.__proxy_url, resource.login, resource.access, self.__proxy_url,
                                  resource.login, resource.access, resource.login)

                    # Describing the connectivity (links)
                    br_parse = iface.bridge.split("->")
                    links += '\t<link client_id="%s">\n\t\t<component_manager name="%s"/>\n' % \
                             (br_parse[1], self._my_urn)

                    # the origin is always a vm
                    origin = resource.external_id + ":" + iface.name
                    links += '\t\t<interface_ref client_id="%s"/>\n' % origin

                    # the destination is always a bridge
                    destination = br_parse[0]
                    links += '\t\t<interface_ref client_id="%s"/>\n' % destination
                    links += '\t\t<link_type name="lan"/>\t</link>\n'

                    # acquiring info to be used in the description of bridges elements
                    bridges_set.add(destination.split(":")[0])
                    bridges_list.append(destination)

                resources += '\t</node>\n'

        for bridge in bridges_set:
            # Describing the resource (a bridge)
            bridges += '\t<node client_id="%s" component_manager_id="%s" exclusive="false">\n\t\t<sliver_type ' \
                       'name="bridge">\n' % (bridge, self._my_urn)

            br_ifaces = [iface for iface in bridges_list if bridge in iface]
            for iface in br_ifaces:
                bridges += '\t\t\t<interface client_id="%s"/>\n' % iface

            bridges += '\t\t</sliver_type>\n\t</node>\n'

        return resources + bridges + links

    def PerformOperationalAction(self, urns, credentials, action, options):
        """
        Perform the specified action (start, stop, etc) on the set slivers.
        :param urns: a list containing the slice and the slivers URNs.
        :param credentials: the user credential (issued by the federation).
        :param action: action to be performed.
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a data structure indicating the result of the action.
        """
        self.expire_slivers()
        self.logger.info('PerformOperationalAction(%r)' % urns)

        # Retrieve the allocation registries
        the_slice, slivers = self.decode_urns(urns)

        # Note this list of privileges is really the name of an operation
        # from the privilege_table in sfa/trust/rights.py
        # Credentials will specify a list of privileges, each of which
        # confers the right to perform a list of operations.
        # EG the 'info' privilege in a credential allows the operations
        # listslices, listnodes, policy
        privileges = (PERFORM_ACTION_PRIV,)
        _ = self.getVerifiedCredentials(the_slice.urn, credentials, options, privileges)

        # A place to store errors on a per-sliver basis.
        # {sliverURN --> "error", sliverURN --> "error", etc.}
        # noinspection PyUnusedLocal
        act_states = []
        # noinspection PyUnusedLocal
        ops_states = []
        if action == 'geni_start':
            act_states = [STATE_GENI_PROVISIONED]
            ops_states = [OPSTATE_GENI_NOT_READY]
        elif action == 'geni_restart':
            act_states = [STATE_GENI_PROVISIONED]
            ops_states = [OPSTATE_GENI_READY]
        elif action == 'geni_stop':
            act_states = [STATE_GENI_PROVISIONED]
            ops_states = [OPSTATE_GENI_READY]
        else:
            msg = "Unsupported: action %s is not supported" % action
            raise ApiErrorException(AM_API.UNSUPPORTED, msg)

        # Handle best effort. Look ahead to see if the operation
        # can be done. If the client did not specify best effort and
        # any resources are in the wrong state, stop and return an error.
        # But if the client specified best effort, trundle on and
        # do the best you can do.
        errors = collections.defaultdict(str)
        for sliver in slivers:
            # ensure that the slivers are provisioned
            if sliver.allocationState() not in act_states or sliver.operationalState() not in ops_states:
                msg = "%d: Sliver %s is not in the right state for action %s."
                msg %= AM_API.UNSUPPORTED, sliver.urn(), action
                errors[sliver.urn()] = msg
        best_effort = False
        if 'geni_best_effort' in options:
            best_effort = bool(options['geni_best_effort'])
        if not best_effort and errors:
            raise ApiErrorException(AM_API.UNSUPPORTED,
                                    "\n".join(errors.values()))

        # Perform the state changes:
        for sliver in slivers:
            if action == 'geni_start':
                if sliver.allocationState() in act_states and sliver.operationalState() in ops_states:
                    sliver.setOperationalState(OPSTATE_GENI_READY)
            elif action == 'geni_restart':
                if sliver.allocationState() in act_states and sliver.operationalState() in ops_states:
                    sliver.setOperationalState(OPSTATE_GENI_READY)
            elif action == 'geni_stop':
                if sliver.allocationState() in act_states and sliver.operationalState() in ops_states:
                    sliver.setOperationalState(OPSTATE_GENI_NOT_READY)
            else:
                # This should have been caught above
                msg = "Unsupported: action %s is not supported" % action
                raise ApiErrorException(AM_API.UNSUPPORTED, msg)

        # Store the status changes
        self.__persistence.update_allocation(slivers)

        return self.successResult([s.status(errors[s.urn()])
                                   for s in slivers])

    def Renew(self, urns, credentials, expiration_time, options):
        """
        Used to increase the lifetime of the experiment.
        It updates the expiration time of the slivers.
        :param urns: a list containing the slice and the slivers URNs.
        :param credentials: the user credential (issued by the federation).
        :param expiration_time: a string containing the expiration time (in UTC with a TZ per RFC3339).
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: False on any error, True on success.
        """
        self.expire_slivers()
        self.logger.info('Renew(%r, %r)' % (urns, expiration_time))

        # Retrieve the allocation registries
        the_slice, slivers = self.decode_urns(urns)

        # Authorization
        privileges = (RENEWSLIVERPRIV,)
        creds = self.getVerifiedCredentials(the_slice.urn, credentials, options, privileges)

        # All the credentials we just got are valid
        expiration = self.min_expire(creds, self.max_lease)
        requested = dateutil.parser.parse(str(expiration_time), tzinfos=tzd)

        # Per the AM API, the input time should be TZ-aware
        # But since the slice cred may not (per ISO8601), convert
        # it to naiveUTC for comparison
        requested = self._naiveUTC(requested)

        # If geni_extend_alap option provided, use the earlier
        # of the requested time and max expiration as the expiration time
        if 'geni_extend_alap' in options and options['geni_extend_alap']:
            if expiration < requested:
                self.logger.info("Got geni_extend_alap: revising slice %s renew request from %s to %s", urns, requested,
                                 expiration)
                requested = expiration

        now = datetime.datetime.utcnow()
        if requested > expiration:
            # Fail the call, the requested expiration exceeds the slice expir.
            msg = (("Out of range: Expiration %s is out of range"
                    + " (past last credential expiration of %s).")
                   % (expiration_time, expiration))
            self.logger.error(msg)
            return self.errorResult(AM_API.OUT_OF_RANGE, msg)
        elif requested < now:
            msg = (("Out of range: Expiration %s is out of range"
                    + " (prior to now %s).")
                   % (expiration_time, now.isoformat()))
            self.logger.error(msg)
            return self.errorResult(AM_API.OUT_OF_RANGE, msg)
        else:
            # Renew all the named slivers
            for sliver in slivers:
                sliver.setExpiration(requested)
                end_time = max(sliver.endTime(), requested)
                sliver.setEndTime(end_time)

        # Store the status changes
        self.__persistence.update_allocation(slivers)

        geni_slivers = [s.status() for s in slivers]
        return self.successResult(geni_slivers)

    def Describe(self, urns, credentials, options):
        """
        Generate a manifest RSpec for the given resources.
        :param urns: a list containing the slice and the slivers URNs.
        :param credentials: the user credential (issued by the federation).
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a dictionary informing the success of the operation.
        """
        self.expire_slivers()
        self.logger.info('Describe(%r)' % urns)

        # APIv3 spec says that a slice with nothing local should
        # give an empty manifest, not an error
        try:
            the_slice, slivers = self.decode_urns(urns)
        except ApiErrorException, ae:
            if ae.code == AM_API.SEARCH_FAILED and "Unknown slice" in ae.output:
                # This is ok
                slivers = []
                the_slice = Slice(urns[0])
            else:
                raise ae

        privileges = (SLIVERSTATUSPRIV,)
        self.getVerifiedCredentials(the_slice.urn, credentials, options, privileges)

        if 'geni_rspec_version' not in options:
            # This is a required option, so error out with bad arguments.
            self.logger.error('No geni_rspec_version supplied to Describe.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version was not supplied.')
        if 'type' not in options['geni_rspec_version']:
            self.logger.error('Describe: geni_rspec_version does not contain a type field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a type field.')
        if 'version' not in options['geni_rspec_version']:
            self.logger.error('Describe: geni_rspec_version does not contain a version field.')
            return self.errorResult(AM_API.BAD_ARGS,
                                    'Bad Arguments: option geni_rspec_version does not have a version field.')

        # Look to see what RSpec version the client requested
        # Error-check that the input value is supported.
        rspec_type = options['geni_rspec_version']['type']
        if isinstance(rspec_type, str):
            rspec_type = rspec_type.lower().strip()
        rspec_version = options['geni_rspec_version']['version']
        if rspec_type != 'geni':
            self.logger.error('Describe: Unknown RSpec type %s requested', rspec_type)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec type %s is not a valid option.' % rspec_type)
        if rspec_version != '3':
            self.logger.error('Describe: Unknown RSpec version %s requested', rspec_version)
            return self.errorResult(AM_API.BAD_VERSION,
                                    'Bad Version: requested RSpec version %s is not a valid option.' % rspec_version)
        self.logger.info("Describe requested RSpec %s (%s)", rspec_type, rspec_version)

        manifest = self.manifest_rspec(the_slice.urn)
        self.logger.debug("Result is now \"%s\"", manifest)
        # Optionally compress the manifest
        if 'geni_compressed' in options and options['geni_compressed']:
            try:
                manifest = base64.b64encode(zlib.compress(manifest))
            except Exception, exc:
                self.logger.error("Error compressing and encoding resource list: %s", traceback.format_exc())
                raise Exception("Server error compressing resource list", exc)
        value = dict(geni_rspec=manifest,
                     geni_urn=the_slice.urn,
                     geni_slivers=[s.status() for s in slivers])
        return self.successResult(value)

    def Shutdown(self, slice_urn, credentials, options):
        """
        Deactivate badly behaving slivers.
        It does not release the resources (for forensics purposes).
        :param slice_urn: the URN of the slice created by the federation.
        :param credentials: the user credential (issued by the federation).
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a dictionary informing the success of the operation.
        """
        self.expire_slivers()
        self.logger.info('Shutdown(%r)' % slice_urn)

        # Authorization
        privileges = (SHUTDOWNSLIVERPRIV,)
        self.getVerifiedCredentials(slice_urn, credentials, options, privileges)

        # Verify the parameters
        the_urn = urn.URN(urn=slice_urn)
        if the_urn.getType() != 'slice':
            self.logger.error('URN %s is not a slice URN.', slice_urn)
            return self.errorResult(AM_API.BAD_ARGS, "Bad Args: Not a slice URN")

        # Retrieve the allocation data
        the_slice, _ = self.decode_urns([slice_urn])

        if the_slice.isShutdown():
            self.logger.error('Slice %s is already shut down.', slice_urn)
            return self.errorResult(AM_API.FORBIDDEN, "Already shut down.")

        # Change the status and save it
        the_slice.shutdown()
        self.__persistence.update_slice(the_slice)

        return self.successResult(True)

    def Delete(self, urns, credentials, options):
        """
        Stop and completely delete the named slivers and/or slice.
        :param urns: a list containing the slice and the slivers URNs.
        :param credentials: the user credential (issued by the federation).
        :param options: a dictionary containing user info and other parameters regarding the RSPEC.
        :return: a dictionary informing the success of the operation.
        """
        self.expire_slivers()
        self.logger.info('Delete(%r)' % urns)

        the_slice, slivers = self.decode_urns(urns)
        privileges = (DELETESLIVERPRIV,)

        self.getVerifiedCredentials(the_slice.urn, credentials, options, privileges)

        # Grab the user_urn
        user_urn = gid.GID(string=options['geni_true_caller_cert']).get_urn()

        # If we get here, the credentials give the caller
        # all needed privileges to act on the given target.
        if the_slice.isShutdown():
            self.logger.info("Slice %s not deleted because it is shutdown", the_slice.urn)
            return self.errorResult(AM_API.UNAVAILABLE, ("Unavailable: Slice %s is unavailable." % the_slice.urn))

        username = user_urn.split("+")[-1]
        # Deleting user from SSH proxy
        try:
            data = {
                "token": self.__ssh_token,
                "DeleteUserSSH": {
                    "username": username
                }
            }
            resp = requests.post(url=self.__ssh_url, json=data)
            json.loads(resp.text)
        except:
            self.logger.warning('delete: failed to remove user from SSH proxy.')

        # Deleting experiment from cloud
        try:
            self.__cloud.purge_project(username, the_slice.urn)
        except:
            self.logger.warning('delete: failed to remove resources from openstack.')

        # Deleting experiment from database
        self.__persistence.delete_experiment(the_slice.urn)

        for sliver in slivers:
            slyce = sliver.slice()
            slyce.delete_sliver(sliver)
            # If slice is now empty, delete it.
            if not slyce.slivers():
                self.logger.debug("Deleting empty slice %r", slyce.urn)
                del self._slices[slyce.urn]

        return self.successResult([s.status() for s in slivers])
