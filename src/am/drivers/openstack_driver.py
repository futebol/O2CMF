# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Diego Giacomelli Cardoso,
# Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from cloud_interface import CloudInterface
import shade

import itertools
import time
from netaddr import *
from scripts import *
from urlparse import urlparse


class OpenstackOperation(CloudInterface):
    def __init__(self):
        shade.simple_logging(debug=False)
        self.cloud = shade.openstack_cloud(cloud='amprod')

        self.auth_url = self.cloud.auth['auth_url']
        self.project_domain_name = self.cloud.auth['project_domain_name']
        self.user_domain_name = self.cloud.auth['user_domain_name']
        self.controller_ip = urlparse(self.auth_url).netloc.split(':')[0]

        self.passwd = 'FUTEBOL@UFES'

        self.user_role = 'user'
        self.provider_network = 'provider'
        self.controller_name = 'controller'

    def __split_name(self, request_name):
        request_list = request_name.split('+')
        final_name = "FUTEBOL+"
        final_name += request_list[-2]
        final_name += "+"
        final_name += request_list[-1]

        return final_name

    def purge_project(self, userName, slice_urn):
        """
        Function that deletes a project from OpenStack, including all it's VMs, networks and routers.
        :param sliceName: A string that contains the slicename, wich is the project name.
        :return: Nothing.
        """
        project_name = self.__split_name(slice_urn)
        project = self.cloud.get_project(name_or_id=project_name)

        if project is None:
            return

        routers = self.cloud.list_routers()
        servers = self.cloud.list_servers(all_projects=True)
        networks = self.cloud.list_networks()
        subnets = self.cloud.list_subnets()
        floating_ips = self.cloud.list_floating_ips()
        security_groups = self.cloud.list_security_groups()
        ports = self.cloud.list_ports()

        for server in servers:
            if server.project_id == project.id:
                print "Deleting Virtual Machine named %s..." % server.name
                self.cloud.delete_server(server)
                print "Done!"

        time.sleep(3)

        print "Deleting floating ips of project %s..." % project.name
        for ip in floating_ips:
            if ip.project_id == project.id:
                self.cloud.delete_floating_ip(ip.id)
        print "Done!"

        time.sleep(3)

        for router, subnet in itertools.product(routers, subnets):
            if (router.project_id == project.id) and (subnet.project_id == project.id):
                print "Deleting interface of %s with subnet %s..." % (router.name, subnet.name)
                try:
                    self.cloud.remove_router_interface(router=router, subnet_id=subnet.id)
                    print "Done!"
                except:
                    print 'Router %s does not have interface with subnet %s.\n' % (router.name, subnet.name)
                    pass

        time.sleep(3)

        for router in routers:
            if router.project_id == project.id:
                print "Deleting router %s..." % router.name
                self.cloud.delete_router(router)
                print "Done."

        time.sleep(3)

        print "Deleting ports of project %s..." % project.name
        for port in ports:
            if port.project_id == project.id:
                self.cloud.delete_port(name_or_id=port.id)
        print "Done!"

        time.sleep(3)

        for network in networks:
            if (network.name != self.provider_network and network.project_id == project.id):
                print "Deleting network %s..." % network.name
                self.cloud.delete_network(network)
                print "Done!"

        time.sleep(3)

        print "Deleting security groups of project %s..." % project.name
        for sec_group in security_groups:
            if (sec_group.project_id == project.id or str(sec_group.project_id) == ""):
                self.cloud.delete_security_group(name_or_id=sec_group.id)
        print "Done!"

        time.sleep(3)

        print "Deleting project %s..." % project.name
        self.cloud.delete_project(project)
        print "Done!"

    def list_raw_resources(self):
        """
        Function that list all resources of the OpenStack cloud. The resources considered are: Hypervisors, images and
        flavors.
        :return: A dictionary with the cloud resources.
        """

        hypervisors = self.cloud.list_hypervisors()

        cloud_resources = dict()

        vcpus = 0
        ram = 0
        disk = 0

        for hyper in hypervisors:
            vcpus += hyper.vcpus - hyper.vcpus_used
            ram += hyper.free_ram_mb
            disk += hyper.free_disk_gb

        cloud_resources["vcpu"] = vcpus
        cloud_resources["ram"] = ram / 1024
        cloud_resources["disk"] = disk

        return cloud_resources

    def verify_resource_availability(self, resources=None):
        """
        Function that verify if a resources is available or not
        :param resources: An object listing resources to be tested.
        :return:
        """
        hypervisors = self.cloud.list_hypervisors()

        numberVM = 0

        for hyper in hypervisors:
            numberVM += hyper.vcpus - hyper.vcpus_used

        return numberVM

    def launch_instance(self, resource, user_cloud, init_script=""):
        """
        Function that create a virtual machine in OpenStack.
        :param resource: An object that contains the name, image, flavor and networks of an instance.
        :param user_cloud: The connection object for access the project.
        :param init_script: An script to be executed in the the VM startup.
        :return: A tuple containing the VM name and it's floating ip.
        """

        image = user_cloud.get_image(name_or_id=resource.image)
        flavor = user_cloud.get_flavor(name_or_id=resource.flavor)

        nics = []

        for net in resource.interfaces:
            network = user_cloud.get_network(name_or_id=net.bridge.split(':')[0])
            port = None
            port = user_cloud.create_port(network_id=str(network.id),
                                          fixed_ips=[{'ip_address': net.address,
                                                      'subnet_id': str(network.subnets[0])}],
                                          )
            nics.append({'net-id': str(network.id), 'port-id': str(port.id)})

        vm = user_cloud.create_server(name=resource.external_id,
                                      flavor=str(flavor.id),
                                      image=str(image.id),
                                      nics=nics,
                                      userdata=init_script,
                                      wait=True,
                                      ip_pool=self.provider_network)

        addrs = vm.addresses[network.name]
        floating_ip = ""
        for net in addrs:
            if 'floating' in net['OS-EXT-IPS:type']:
                floating_ip = net['addr']

        return resource.external_id, str(floating_ip)

    def create_network(self, bridge, user_cloud):
        """
        Function that creates a network in OpenStack.
        :param bridge: An object that contains the bridge parameters.
        :param user_cloud: Connection Object to OpenStack.
        :return: An object of the network created.
        """

        bridge_name = bridge.bridge.split(':')[0]
        network = user_cloud.create_network(name=bridge_name)

        network_ip = IPNetwork(bridge.address + "/24")
        cidr = str(network_ip.network) + "/" + str(network_ip.prefixlen)

        index = cidr.find('/')
        gateway = cidr[:index - 1]
        gateway = str(gateway) + '1'

        ip_version = '4'

        user_cloud.create_subnet(subnet_name='sub_' + str(network.name),
                                 network_name_or_id=str(network.id),
                                 ip_version=ip_version,
                                 cidr=cidr,
                                 gateway_ip=gateway,
                                 enable_dhcp=True,
                                 dns_nameservers=['8.8.8.8'])

        network = user_cloud.get_network(name_or_id=bridge.name)

        return network

    def create_router(self, router_name, user_cloud):
        """
        A function that creates a router in OpenStack.
        :param router_name: A string that contains a name for the router.
        :param user_cloud: Connection object to OpenStack.
        :return: An object containing the router.
        """

        network = user_cloud.get_network(name_or_id=self.provider_network)
        router = user_cloud.create_router(name=router_name, ext_gateway_net_id=str(network.id))
        return router

    def add_router_interface(self, router_name, network_name, user_cloud):
        """
        Function that creates an interface between a router and a network.
        :param router_name: A string that contains the router name.
        :param network_name: A string that contains the network name.
        :param user_cloud: Connection object to OpenStack.
        :return: Nothing
        """

        router = user_cloud.get_router(name_or_id=str(router_name))
        network = user_cloud.get_network(name_or_id=str(network_name))

        user_cloud.add_router_interface(router, subnet_id=str(network.subnets[0]))

    def setup_project(self, request, ssh_keys):
        print "Collecting project and user data..."
        project_name = self.__split_name(request["name"])

        project = self.cloud.create_project(name=project_name,
                                            domain_id='default',
                                            description="Slice of FUTEBOL UFES Testbed.")

        user_name = request["user"].name

        user = self.cloud.get_user(name_or_id=user_name)
        if user is None:
            user = self.cloud.create_user(name=user_name,
                                          password=self.passwd,
                                          domain_id='default',
                                          default_project=project_name)

        self.cloud.grant_role(name_or_id=self.user_role, user=user_name, project=project_name)

        user_cloud = self.cloud.connect_as(username=user_name,
                                           password=self.passwd,
                                           project_name=project_name,
                                           auth_url=self.auth_url,
                                           project_domain_name=self.project_domain_name,
                                           user_domain_name=self.user_domain_name)

        print "Done!"

        print "Collecting and initializing request configuration parameters..."
        resources = request["resources"].values()

        init_script = HEADER_SCRIPT
        user_keys = "\n".join(ssh_keys)
        init_script += DEFAULT_SCRIPT.format(user_name, user_keys)

        bridges_list = []
        for item in resources:
            bridge_name = item.interfaces[0].bridge.split(':')[0]
            if not any(bridge_name in x.bridge for x in bridges_list):
                bridges_list.append(item.interfaces[0])

        print "Creating router..."
        router_name = "router+" + user_name
        self.create_router(router_name, user_cloud)
        print "Done!"

        print "Creating networks..."
        for bridge in bridges_list:
            self.create_network(bridge, user_cloud)
            self.add_router_interface(router_name, bridge.bridge.split(':')[0], user_cloud)
        print "Done!"

        project_resources = dict()
        print "Creating virtual machines..."
        for resource in resources:
            vm_name, floating_ip = self.launch_instance(resource, user_cloud, init_script)
            project_resources[vm_name] = floating_ip
        print "Done!"

        sec_group_id = 'default'
        for item in user_cloud.list_security_groups():
            if item.project_id == project.id:
                sec_group_id = item.id
        user_cloud.create_security_group_rule(secgroup_name_or_id=sec_group_id,
                                              direction='ingress',
                                              remote_ip_prefix='0.0.0.0/0',
                                              protocol='tcp',
                                              port_range_max='22',
                                              port_range_min='22',
                                              ethertype='IPv4')

        user_cloud.create_security_group_rule(secgroup_name_or_id=sec_group_id,
                                              direction='ingress',
                                              remote_ip_prefix='0.0.0.0/0',
                                              protocol='icmp',
                                              port_range_max=None,
                                              port_range_min=None,
                                              ethertype='IPv4')

        return project_resources

    def turn_on_vms(self):
        pass

    def reboot(self, project_urn, vm_name):
        pass
        # conn = self.__get_connection()
        # session = self.__get_credential_admin()
        # nova = Nova.Client(**session)
        # project_name = self.__split_name(project_urn)
        # project = conn.identity.find_project(name_or_id=project_name)
        # list = nova.servers.list(search_opts={'tenant_id': project.id, 'all_tenants': 1})
        #
        # for item in list:
        #     if item.name in vm_name:
        #         item.reboot("SOFT")

    def list_image_per_flavor(self):
        resources = dict()

        images_list = self.cloud.list_images()
        flavors_list = self.cloud.list_flavors()

        for flavor in flavors_list:
            images_allowed = list()
            for image in images_list:
                if ((flavor.disk >= image.min_disk) and
                        (flavor.ram >= image.min_disk)):
                    images_allowed.append(str(image.name))
            resources[flavor.name] = images_allowed

        return resources

    def get_defaults(self):

        default_image = 'urn:publicid:IDN+futebol.inf.ufes.br+image+img-ubuntu16-cloud'
        default_flavor = 'm1.small'

        return default_flavor, default_image
