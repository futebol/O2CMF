# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from abc import ABCMeta, abstractmethod


class CloudInterface:
    __metaclass__ = ABCMeta

    @abstractmethod
    def list_raw_resources(self):
        """
        This method returns the amount of vCPUs, RAM memory and disk available in the cloud.
        :return: dict containing the resource (as key) and the the available amount (as value).
        The keys are: 'vcpu', 'ram' and 'disk'.
        The amount of RAM memory and disk are calculated in GB.
        """
        pass

    @abstractmethod
    def list_image_per_flavor(self):
        """
        This method returns the images suitable for each flavor.
        :return: dict containing the flavor name (as key) and a list of image names suitable for the flavor (as value).
        """
        pass

    @abstractmethod
    def get_defaults(self):
        """
        This method returns the default flavor and image.
        :return: tuple containing: (the flavor name, the image name).
        """
        pass

    @abstractmethod
    def verify_resource_availability(self, request):
        """
        This method checks if there are enough resources to fulfill the specified request.
        :param request: a dict indicating what resources the user wants to create in the cloud.
        The dict must follow the template: {'name': the project, 'user': an instance of User class, 'resources': {}}
        The value linked to the 'resources' key must be a dict which the values may contain: at most, one instance of
        Tenant class; one or more instances of VirtualMachine class; and, eventually, one or more instances of
        VirtualNetworkFunction class. The keys are the resource's external_id (aka, client_id).
        :return: boolean indicating if there are enough resources.
        """
        pass

    @abstractmethod
    def setup_project(self, request, ssh_key):
        """
        This method creates a project in the cloud (instantiating the specified resources).
        :param request: a dict indicating what resources the user wants to create in the cloud. It is the same data
        structure described in the verify_resource_availability method. The dict must follow the template:
        {'name': the project, 'user': an instance of User class, 'resources': {}}
        The value linked to the 'resources' key must be a dict which the values may contain: at most, one instance of
        Tenant class; one or more instances of VirtualMachine class; and, eventually, one or more instances of
        VirtualNetworkFunction class. The keys are the resource's external_id (aka, client_id).
        For each virtual machine specified in 'resources' dict, a floating IP should be provided (for SSH access).
        An additional comment: if there is a VM using the orchestrator image in the 'resources' dict, this VM should be
        charged with the RC file an the NFV endpoint address (in an environmental variable called 'OS_NFV_ENDPOINT').
        :param ssh_key: list of user's public keys to be used for authentication in the SSH access.
        :return: dict containing the VM external_id (as key) and the assigned floating IP (as value).
        """
        pass

    @abstractmethod
    def reboot(self, project_name, resource_name):
        """
        This method reboots an VM instance in a given project in the cloud.
        :param project_name: the slice name (URN).
        :param resource_name: the 'external_id' of the VM.
        :return: None
        """
        pass

    @abstractmethod
    def purge_project(self, user_name, project_name):
        """
        This method deletes a project (and all instantiated resources) in the cloud.
        :param user_name: the name of the project owner (the experimenter).
        :param project_name: the slice name (URN).
        :return: None
        """
        pass
