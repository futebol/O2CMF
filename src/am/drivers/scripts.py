# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Diego Giacomelli Cardoso
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

HEADER_SCRIPT = """#!/bin/bash"""
DEFAULT_SCRIPT = """
NEWUSERNAME="{0}"
if [ -f /usr/sbin/setenforce ];then
    /usr/sbin/setenforce 0
    echo "SELINUX=disabled" > /etc/sysconfig/selinux
    echo "SELINUXTYPE=targeted" >> /etc/sysconfig/selinux
fi

useradd -s /bin/bash -md /home/$NEWUSERNAME $NEWUSERNAME
groupadd $NEWUSERNAME
mkdir -p /home/$NEWUSERNAME/.ssh/
echo "{1}" > /home/$NEWUSERNAME/.ssh/authorized_keys
chown -R $NEWUSERNAME /home/$NEWUSERNAME/.ssh/
chgrp -R $NEWUSERNAME /home/$NEWUSERNAME/.ssh/
chmod 700 /home/$NEWUSERNAME/.ssh/
chmod 600 /home/$NEWUSERNAME/.ssh/authorized_keys
echo "$NEWUSERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/90-cloud-nerds-init-users
"""


ORCHESTRATOR_SCRIPT = """
echo '{1} {2}' >> /etc/hosts
echo "{0}" >> /usr/local/user-openrc.sh
echo 'source /usr/local/user-openrc.sh' >> /etc/bash.bashrc
source /usr/local/user-openrc.sh
"""

RC_SCRIPT = """
#!/usr/bin/env bash

# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
#
# *NOTE*: Using the 3 *Identity API* does not necessarily mean any other
# OpenStack API is version 3. For example, your cloud provider may implement
# Image API v1.1, Block Storage API v2, and Compute API v2.0. OS_AUTH_URL is
# only for the Identity API served through keystone.
export OS_AUTH_URL={4}

# With the addition of Keystone we have standardized on the term **project**
# as the entity that owns the resources.
export OS_PROJECT_ID={0}
export OS_PROJECT_NAME={1}
export OS_USER_DOMAIN_NAME="Default"
export OS_PROJECT_DOMAIN_ID="Default"

# unset v2.0 items in case set
unset OS_TENANT_ID
unset OS_TENANT_NAME

# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_USERNAME="{2}"

# With Keystone you pass the keystone password.
export OS_PASSWORD="{3}"

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME="RegionOne"
# Don't leave a blank variable, unset it if it was empty

export OS_INTERFACE=public
export OS_IDENTITY_API_VERSION=3

#export OS_NFV_ENDPOINT="."

"""

TACKER_EXEC = """'{0} {1}'"""
