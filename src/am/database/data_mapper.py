# coding: utf-8
# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Rafael Silva Guimarães
# Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from sqlalchemy import Column, DateTime, ForeignKey, Integer, LargeBinary, String, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(30), nullable=False, unique=True)
    urn = Column(String(120), nullable=False, unique=True)


class Slice(Base):
    __tablename__ = 'slice'

    id = Column(Integer, primary_key=True, unique=True)
    urn = Column(String(120), nullable=False, unique=True)
    shutdown = Column(Integer, nullable=False, server_default=text("'0'"))
    user_id = Column(ForeignKey(u'user.id'), nullable=False, index=True)

    user = relationship(u'User')


class CloudRequest(Base):
    __tablename__ = 'cloud_request'

    slice_id = Column(ForeignKey(u'slice.id'), primary_key=True, unique=True)
    slice_urn = Column(String(120), nullable=False, unique=True)
    request = Column(LargeBinary, nullable=False)


class Sliver(Base):
    __tablename__ = 'sliver'

    resource_id = Column(ForeignKey(u'resource.id'), primary_key=True)
    expiration = Column(DateTime, nullable=False)
    start_time = Column(DateTime, nullable=False)
    end_time = Column(DateTime, nullable=False)
    allocation_state = Column(String(30), nullable=False)
    operational_state = Column(String(30), nullable=False)
    urn = Column(String(120), nullable=False, unique=True)
    shutdown = Column(Integer, nullable=False, server_default=text("'0'"))
    slice_id = Column(ForeignKey(u'slice.id'), nullable=False, index=True)

    slice = relationship(u'Slice')


class Resource(Base):
    __tablename__ = 'resource'

    id = Column(Integer, primary_key=True, unique=True)
    type = Column(String(30), nullable=False)
    available = Column(Integer, nullable=False, server_default=text("'1'"))
    external_id = Column(String(45), nullable=False)
    resource_state = Column(String(30), nullable=False)


class VM(Base):
    __tablename__ = 'vm'

    resource_id = Column(ForeignKey(u'resource.id'), primary_key=True, unique=True)
    image = Column(String(90), nullable=False)
    flavor = Column(String(15), nullable=False)
    login = Column(String(30))
    access = Column(String(45))


class NetIface(Base):
    __tablename__ = 'net_iface'

    id = Column(Integer, primary_key=True, unique=True)
    name = Column(String(45), nullable=False)
    type = Column(String(45), nullable=False)
    address = Column(String(45), nullable=False)
    netmask = Column(String(45), nullable=False)
    bridge = Column(String(45), nullable=False)
    vm_resource_id = Column(ForeignKey(u'vm.resource_id'), nullable=False, index=True)

    vm = relationship(u'VM')
