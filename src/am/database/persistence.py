# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

import os

from gcf.geni.config import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import logging


from data_manager import *


class Persistence:
    """
    Class that intermediates tha access to the persistence mechanisms.
    It is used to simplify the integration between the AM and the ORM.
    """

    def __init__(self):
        file_path = os.environ["AM_PATH"] + "config.ini"
        config = read_config(file_path)
        url_pattern = "%s+%s://%s:%s@%s:%s/%s"
        url = url_pattern % (
            config["mysql"]["dialect"],
            config["mysql"]["driver"],
            config["mysql"]["username"],
            config["mysql"]["password"],
            config["mysql"]["host"],
            config["mysql"]["port"],
            config["mysql"]["database"])
        engine = create_engine(url)
        logging.basicConfig()
        logging.getLogger('sqlalchemy').setLevel(logging.ERROR)
        session = sessionmaker(bind=engine)
        self._session = session()

    def retrieve_slices(self):
        """
        Retrieves all slices stored in the database.
        :return: a dict of slices (key = urn and value = slice).
        """
        return SliceDAO.read_all(self._session)

    def delete_expired_allocations(self):
        """
        Deletes from the database all slices (and its associated objects) that already expired the reservation slot.
        :return: a list of tuples containing the username and URNs of the deleted slices.
        """
        info = SliverDAO.delete_expired(self._session)
        self._session.commit()
        return info

    def save_allocation(self, user, newslice):
        """
        Calls the persistence mechanisms to store the user data and its reservation.
        :param user: the user which will perform the experiment.
        :param newslice: a slice which specifies the experiment's time slot and resources (e.g. the reservation).
        :return: None
        """
        # noinspection PyUnusedLocal
        user_db = None
        try:
            user_db = UserDAO.read_by_name(self._session, user.name)
        except:
            user_db = UserDAO.create(self._session, user)
        SliceDAO.create(self._session, newslice, user_db.id)
        self._session.commit()

    def save_future_cloud_request(self, cloud_request):
        """
        Saves a request to be sent to the cloud.
        :param cloud_request: the request.
        :return: None
        """
        CloudRequestDAO.create(self._session, cloud_request)
        self._session.commit()

    def retrieve_slice(self, slice_urn):
        """
        Retrieves a slice based on its URN.
        :param slice_urn: the URN which identifies the slice.
        :return: instance of slice.
        """
        return SliceDAO.read_by_urn(self._session, slice_urn)

    def update_slice(self, the_slice):
        """
        Updates (only) the slice data in the database.
        :param the_slice: the slice updated.
        :return: None
        """
        SliceDAO.update_only_itself(self._session, the_slice)
        self._session.commit()

    def update_allocation(self, slivers):
        """
        Updates the slivers' state in the database.
        :param slivers: a list of slivers to be updated.
        :return: None
        """
        for sliv in slivers:
            SliverDAO.update(self._session, sliv)
        self._session.commit()

    def retrieve_cloud_request(self, slice_urn):
        """
        Retrieves from database a request to be sent to the cloud.
        :param slice_urn: the URN which identifies the slice.
        :return: None
        """
        request = CloudRequestDAO.read(self._session, slice_urn)
        return request

    def delete_cloud_request(self, slice_urn):
        """
        Removes from database a request sent to the cloud.
        :param slice_urn: the URN which identifies the slice.
        :return: None
        """
        CloudRequestDAO.delete(self._session, slice_urn)
        self._session.commit()

    def delete_experiment(self, slice_urn):
        """
        Deletes a slice, its slivers and associated resources from database.
        :param slice_urn: the URN which identifies the slice.
        :return: None
        """
        SliceDAO.delete(self._session, slice_urn)
        self._session.commit()
