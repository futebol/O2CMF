# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Isabella de Albuquerque Ceravolo
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

from data_mapper import *
from gcf.geni.am.resource import Resource as ResourceTestbed
from gcf.geni.am.am3 import Slice as SliceTestbed
from gcf.geni.am.am3 import Sliver as SliverTestbed
from testbed_resources.user import User as UserTestbed
from testbed_resources.vm import VirtualMachine as VMTestbed
from testbed_resources.vm import NetworkInterface
import datetime
import pickle


# noinspection PyClassHasNoInit
class UserDAO:
    """
    Class responsible for the persistence of User objects.
    """

    @staticmethod
    def create(session, user):
        """
        Saves the user's data in the database.
        :param session: the database session.
        :param user: the user instance to be persisted.
        :return: the persisted object (updated).
        """
        user_db = User(name=user.name, urn=user.urn)
        session.add(user_db)
        session.flush()
        user.id = user_db.id
        return user

    @staticmethod
    def read_by_name(session, name):
        """
        Retrieves the user's info from database.
        :param session: the database session.
        :param name: parameter used to identify the user in the database.
        :return: a user's instance containing the desired data.
        """
        result = session.query(User).filter(User.name == name).first()
        user = UserTestbed(result.name, result.urn)
        user.id = result.id
        return user

    @staticmethod
    def read_by_id(session, identifier):
        """
        Retrieves the user's info from database.
        :param session: the database session.
        :param identifier: the id number of the record in the database.
        :return: a user's instance containing the desired data.
        """
        result = session.query(User).filter(User.id == identifier).first()
        user = UserTestbed(result.name, result.urn)
        user.id = result.id
        return user


# noinspection PyClassHasNoInit
class SliceDAO:
    """
    Class responsible for the persistence of Slice objects.
    """

    @staticmethod
    def create(session, new_slice, user_id):
        """
        Saves the slice's data in the database.
        :param session: the database session.
        :param new_slice: a slice's instance to be persisted
        :param user_id: the id of the user who own the slice.
        :return: None
        """
        slice_db = Slice(urn=new_slice.urn, shutdown=new_slice.isShutdown(), user_id=user_id)
        session.add(slice_db)
        session.flush()
        SliverDAO.create_multiple(session, new_slice.slivers(), slice_db.id)

    @staticmethod
    def read_all(session):
        """
        Retrieve all slices stored in the database.
        :param session: the database session.
        :return: a dict containing all slices.
        """
        slices = dict()
        result_set = session.query(Slice).all()
        for result in result_set:
            current_slice = SliceTestbed(result.urn)
            current_slice.id = result.id
            current_slice._shutdown = result.shutdown
            current_slice._slivers = SliverDAO.read_by_slice(session, current_slice)
            slices[result.urn] = current_slice
        return slices

    @staticmethod
    def delete_by_id(session, identifier):
        """
        Deletes the slice's data from database using its key.
        :param session: the database session.
        :param identifier: the key (an integer).
        :return: a pair (python tuple) containing the username of slice's owner and the slice's URN.
        """
        result = session.query(Slice).filter(Slice.id == identifier).first()
        urn = result.urn
        try:
            CloudRequestDAO.delete(session, urn)
        except:
            pass
        username = UserDAO.read_by_id(session, result.user_id).name
        session.delete(result)
        return username, urn

    @staticmethod
    def read_by_urn(session, slice_urn):
        """
        Retrieves a slice based on its URN.
        :param session: the database session.
        :param slice_urn: the slice's URN.
        :return: a slice.
        """
        result = session.query(Slice).filter(Slice.urn == slice_urn).first()
        current_slice = SliceTestbed(result.urn)
        current_slice.id = result.id
        current_slice._shutdown = result.shutdown
        current_slice._slivers = SliverDAO.read_by_slice(session, current_slice)
        return current_slice

    @staticmethod
    def update_only_itself(session, the_slice):
        """
        Updates the 'shutdown' column in the database.
        :param session: the database session.
        :param the_slice: the slice to be updated.
        :return: None
        """
        result = session.query(Slice).filter(Slice.urn == the_slice.getURN()).first()
        result.shutdown = the_slice.isShutdown()
        session.flush()

    @staticmethod
    def delete(session, slice_urn):
        """
        Deletes the slice (and all related data) from database using its URN.
        :param session: the database session.
        :param slice_urn: the slices' URN.
        :return: None
        """
        try:
            CloudRequestDAO.delete(session, slice_urn)
        except:
            pass
        result = session.query(Slice).filter(Slice.urn == slice_urn).first()
        SliverDAO.delete_by_slice_id(session, result.id)
        session.delete(result)


# noinspection PyClassHasNoInit
class CloudRequestDAO:
    """
    Class responsible for the persistence of future requests for the cloud.
    """

    @staticmethod
    def create(session, request):
        """
        Saves the data structure representing a request for the cloud.
        It is a kind of temporary data.
        :param session: the database session.
        :param request: a future request for the cloud.
        :return: None
        """
        slice = SliceDAO.read_by_urn(session, request["name"])
        request_binary = pickle.dumps(request)
        request_db = CloudRequest(slice_id=slice.id, slice_urn=request["name"], request=request_binary)
        session.add(request_db)

    @staticmethod
    def read(session, slice_urn):
        """
        Retrieves the data structure representing a request for the cloud.
        :param session: the database session.
        :param slice_urn: the URN of the slice which the request belongs to.
        :return: the data structure representing a request (a dictionary).
        """
        result = session.query(CloudRequest).filter(CloudRequest.slice_urn == slice_urn).first()
        request = pickle.loads(result.request)
        return request

    @staticmethod
    def delete(session, slice_urn):
        """
        Removes from database the data structure representing a request for the cloud.
        :param session: the database session.
        :param slice_urn: the URN of the slice which the request belongs to.
        :return: None
        """
        result = session.query(CloudRequest).filter(CloudRequest.slice_urn == slice_urn).first()
        session.delete(result)


# noinspection PyClassHasNoInit
class SliverDAO:
    """
    Class responsible for the persistence of Sliver objects.
    """

    @staticmethod
    def create_multiple(session, sliver_list, slice_id):
        """
        Saves a list of slivers.
        :param session: the database session.
        :param sliver_list: a list of sliver instances to be persisted.
        :param slice_id: the identifier of the slice that owns the slivers.
        :return: None
        """
        for sliv in sliver_list:
            resource_id = _ResourceDAO.create(session, sliv.resource())
            sliver_db = Sliver(resource_id=resource_id, expiration=sliv.expiration(), start_time=sliv.startTime(),
                               end_time=sliv.endTime(), allocation_state=sliv.allocationState(),
                               operational_state=sliv.operationalState(), urn=sliv.urn(), shutdown=sliv.isShutdown(),
                               slice_id=slice_id)
            session.add(sliver_db)

    @staticmethod
    def read_by_slice(session, the_slice):
        """
        Retrieves all slivers owned by the given slice.
        :param session: the database session.
        :param the_slice: the slice that owns the slivers.
        :return: a list of slivers.
        """
        slivers = list()
        result_set = session.query(Sliver).filter(Sliver.slice_id == the_slice.id).all()
        for result in result_set:
            resource = _ResourceDAO.read_by_id(session, result.resource_id)
            sliver = SliverTestbed(the_slice, resource)
            sliver._id = result.resource_id
            sliver.setExpiration(result.expiration)
            sliver.setStartTime(result.start_time)
            sliver.setEndTime(result.end_time)
            sliver.setAllocationState(result.allocation_state)
            sliver.setOperationalState(result.operational_state)
            sliver._urn = result.urn
            sliver._shutdown = result.shutdown
            slivers.append(sliver)
        return slivers

    @staticmethod
    def delete_expired(session):
        """
        Deletes all slivers (and its related data) that have an invalid time slot.
        :param session: the database session.
        :return: a list of tuples containing the name of the owner of the expired slice and the slice's URN.
        """
        slices_id = set()
        info_list = list()
        now = datetime.datetime.utcnow()
        result_set = session.query(Sliver).filter(Sliver.expiration <= now).all()
        for result in result_set:
            resource_id = result.resource_id
            slices_id.add(result.slice_id)
            session.delete(result)
            _ResourceDAO.delete_by_id(session, resource_id)
        for identifier in slices_id:
            # info represents a tuple with the name of the user that owns the slice and the slice urn.
            info = SliceDAO.delete_by_id(session, identifier)
            info_list.append(info)
        return info_list

    @staticmethod
    def update(session, sliv):
        """
        Updates the sliver data and all related data.
        :param session: the database session.
        :param sliv: updated sliver.
        :return: None
        """
        # noinspection PyProtectedMember
        result = session.query(Sliver).filter(Sliver.resource_id == sliv._id).first()
        result.expiration = sliv.expiration()
        result.start_time = sliv.startTime()
        result.end_time = sliv.endTime()
        result.allocation_state = sliv.allocationState()
        result.operational_state = sliv.operationalState()
        result.urn = sliv.urn()
        result.shutdown = sliv.isShutdown()
        session.flush()
        _ResourceDAO.update(session, sliv.resource())

    @staticmethod
    def delete_by_slice_id(session, slice_id):
        """
        Deletes the slice's data from database using its key.
        :param session: the database session.
        :param slice_id: the key (an integer) of the slice which owns slivers.
        :return: None
        """
        result_set = session.query(Sliver).filter(Sliver.slice_id == slice_id).all()
        for result in result_set:
            resource_id = result.resource_id
            session.delete(result)
            _ResourceDAO.delete_by_id(session, resource_id)


# noinspection PyClassHasNoInit
class _ResourceDAO:
    # Class responsible for the persistence of the attributes inherited from Resource class.

    @staticmethod
    def create(session, resource):
        # Saves the resource's data in the database.

        resource_db = Resource(type=resource.type, available=resource.available,
                               external_id=resource.external_id, resource_state=resource.state)
        session.add(resource_db)
        session.flush()
        res_id = resource_db.id
        if resource.type == "vm":
            _VirtualMachineDAO.create(session, res_id, resource)
        else:
            raise Exception
        return res_id

    @staticmethod
    def read_by_id(session, identifier):
        # Retrieves the resource's data from database using its key.

        result = session.query(Resource).filter(Resource.id == identifier).first()
        parent_resource = ResourceTestbed(result.id, result.type)
        parent_resource.available = result.available
        parent_resource.external_id = result.external_id
        parent_resource.state = result.resource_state
        # noinspection PyUnusedLocal
        resource = None
        if parent_resource.type == "vm":
            resource = _VirtualMachineDAO.read_by_id(session, parent_resource)
        else:
            raise Exception
        return resource

    @staticmethod
    def update(session, resource):
        # Updates the child resource data.

        if resource.type == "vm":
            _VirtualMachineDAO.update(session, resource)

    @staticmethod
    def delete_by_id(session, identifier):
        # Deletes the resource's data from database using its key.

        result = session.query(Resource).filter(Resource.id == identifier).first()
        if result.type == "vm":
            _VirtualMachineDAO.delete_by_id(session, result.id)
        else:
            raise Exception
        session.delete(result)


# noinspection PyClassHasNoInit
class _VirtualMachineDAO:
    # Class responsible for the persistence of VM objects.

    @staticmethod
    def create(session, parent_id, child_resource):
        # Saves the VM's data in the database.

        vm_db = VM(resource_id=parent_id, image=child_resource.image, flavor=child_resource.flavor)
        session.add(vm_db)
        session.flush()
        for iface in child_resource.interfaces:
            iface_db = NetIface(name=iface.name, type=iface.type, address=iface.address, netmask=iface.netmask,
                                bridge=iface.bridge, vm_resource_id=vm_db.resource_id)
            session.add(iface_db)

    @staticmethod
    def read_by_id(session, parent_resource):
        # Retrieves the VM's data from database using its key.

        result_vm = session.query(VM).filter(VM.resource_id == parent_resource.id).first()
        vm = VMTestbed()
        vm.id = parent_resource.id
        vm.image = result_vm.image
        vm.flavor = result_vm.flavor
        vm.login = result_vm.login
        vm.access = result_vm.access
        result_set_ifaces = session.query(NetIface).filter(NetIface.vm_resource_id == parent_resource.id).all()
        for iface in result_set_ifaces:
            vm_iface = NetworkInterface(iface.name, iface.type, iface.address, iface.netmask)
            vm_iface.bridge = iface.bridge
            vm.interfaces.append(vm_iface)
        vm.merge(parent_resource)
        return vm

    @staticmethod
    def update(session, resource):
        # Updates the VM data adding login (username) and access (IP address).

        result = session.query(VM).filter(VM.resource_id == resource.id).first()
        result.login = resource.login
        result.access = resource.access
        session.flush()

    @staticmethod
    def delete_by_id(session, identifier):
        # Deletes the VM's data from database using its key.

        result_set_ifaces = session.query(NetIface).filter(NetIface.vm_resource_id == identifier).all()
        for iface in result_set_ifaces:
            session.delete(iface)
        session.flush()
        result_vm = session.query(VM).filter(VM.resource_id == identifier).first()
        session.delete(result_vm)
        session.flush()
