#!/bin/bash
# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Rafael Silva Guimarães
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------

GUNICORN=$(which gunicorn 2> /dev/null)
APP_PATH="/usr/local/proxyssh/"
PIDFILE="/var/run/proxyssh.pid"

start() {
    echo "Starting ProxySSH Agent..."
    if [ -x $GUNICORN ]
    then
        cd $APP_PATH
        $GUNICORN -D -p $PIDFILE -b 0.0.0.0:8080 app_web:app
        echo "Application now is running..."
    else
        echo "Application cannot start: Gunicorn not found!"
    fi
}

stop() {
    if [ -f $PIDFILE ]
    then
        kill $(cat $PIDFILE)
    else
        echo "PIDFILE not found!"
    fi
}

restart(){
    stop
    start
}

status(){
    echo
}

case "$1" in
    start)
        start
        RETVAL=$?
        ;;
    stop)
        stop
        RETVAL=$?
        ;;
    restart)
        restart
        RETVAL=$?
        ;;
    status)
        RETVAL=$?
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart}"
        RETVAL=2
esac
