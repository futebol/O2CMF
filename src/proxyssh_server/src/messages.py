#!/usr/bin/python
# coding: utf-8
# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Rafael Silva Guimarães
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------
import grp
import os
import pwd
from datetime import datetime

import sh

import config
import response as resp


class Message(object):

    def __init__(self):
        pass

    def execute(self):
        """
        Perform instructions
        """
        raise NotImplementedError


class ErrorMessage(Message):

    def __init__(self, error=None):
        self.error = error

    def execute(self):
        result = resp.ERRORMSG
        result["error"]["reason"] = self.error["reason"]
        result["error"]["message"] = self.error["message"]
        result["error"]["code"] = self.error["code"]
        result["error"]["time"] = datetime.now()
        return result


class ListUsersSSHMessage(Message):
    """
        List users added for Proxy SSH
    """
    def __init__(self, error=None):
        self.error = error
        
    def execute(self):
        """
        Perform instructions
        """
        raise NotImplementedError


class DeleteUserSSHMessage(Message):
    """
        Delete user in Proxy SSH
    """
    def __init__(self, message=None):
        self.message = message

    def execute(self):
        """
        Perform instructions
        """
        username = self.message["DeleteUserSSH"]["username"]
        # Adding user to system
        try:
            userdel = sh.userdel.bake("-r")
            res = userdel(username)

            result = resp.COMMANDOK
            result["status"]["code"] = 1
            result["status"]["name"] = "DeleteUserSSH"
            result["status"]["time"] = datetime.now()
            return result
        except sh.ErrorReturnCode as error:
            return ErrorMessage(
                {"reason": "DeleteUserSSH", "message": "{}".format(error.stderr), "code": 500}
            ).execute()


class AddUserSSHMessage(Message):
    """
    Add user with its public ssh-key.
    """
    def __init__(self, message=None):
        self.message = message

    def execute(self):
        # Using username and ssh_key to create user
        username = self.message["CreateUserSSH"]["username"]
        ssh_key = self.message["CreateUserSSH"]["ssh-key"]
        # Adding user to system
        try:
            adduser = sh.adduser.bake("--disabled-password", gecos=" ")
            res = adduser(username)
        except sh.ErrorReturnCode as error:
            res = None
            return ErrorMessage(
                {"reason": "CreateUserSSH", "message": "{}".format(error.stderr), "code": 500}
            ).execute()
        # Adding ssh-key
        if res:
            root_dir = os.path.expanduser(
                os.path.join(
                    '/home/{}'.format(username), '.ssh'
                )
            )
            if os.path.exists(root_dir):
                pass
            else:
                # creating directory
                uid = pwd.getpwnam(username).pw_uid
                gid = grp.getgrnam(username).gr_gid
                os.makedirs(root_dir)
                os.chown(root_dir, uid, gid)
                os.chmod(root_dir, 0700)

            # add key
            file_ssh = "{}/{}".format(root_dir, "authorized_keys")
            with open(file_ssh, "a+") as akeys:
                if isinstance(ssh_key, list):
                    for k in ssh_key:
                        akeys.write("{}\n".format(k))
                elif isinstance(ssh_key, str):
                    akeys.write("{}\n".format(ssh_key))
                os.chown(file_ssh, uid, gid)
                os.chmod(file_ssh, 0644)

        result = resp.COMMANDOK
        result["status"]["code"] = 1
        result["status"]["name"] = "CreateUserSSH"
        result["status"]["time"] = datetime.now()
        return result


class AddSshKeyMessage(Message):
    """
    this class is for adding another Ssh-pub Key to the user.
    """
    def __init__(self, message=None):
        self.message = message

    def execute(self):
        # Using username and ssh_key to create user
        username = self.message["AddSshKey"]["username"]
        ssh_key = self.message["AddSshKey"]["ssh-key"]
        # Adding user to system
        try:
            # Adding ssh-key
            root_dir = os.path.expanduser(
                os.path.join(
                    '/home/{}'.format(username), '.ssh'
                )
            )
            if os.path.exists(root_dir):
                pass
            else:
                # user don't exist
                result = resp.ERRORMSG
                result["status"]["code"] = 0
                result["status"]["name"] = "AddSshKey"
                result["status"]["time"] = datetime.now()
                return result

            # adding ssh-pub key
            file_ssh = "{}/{}".format(root_dir, "authorized_keys")
            with open(file_ssh, "a+") as akeys:
                if isinstance(ssh_key, list):
                    for k in ssh_key:
                        akeys.write("{}\n".format(k))
                elif isinstance(ssh_key, str):
                    akeys.write("{}\n".format(ssh_key))
                uid = pwd.getpwnam(username).pw_uid
                gid = grp.getgrnam(username).gr_gid
                os.chown(file_ssh, uid, gid)
                os.chmod(file_ssh, 0644)

        except sh.ErrorReturnCode as error:
            res = None
            return ErrorMessage(
                {"reason": "AddSshKey", "message": "{}".format(error.stderr), "code": 500}
            ).execute()

        result = resp.COMMANDOK
        result["status"]["code"] = 1
        result["status"]["name"] = "AddSshKey"
        result["status"]["time"] = datetime.now()
        return result

class FactoryMessage(object):
    @classmethod
    def handle(cls, msg=""):
        try:
            if "token" in msg:
                if msg["token"] == config.TOKEN:
                    if "CreateUserSSH" in msg:
                        return AddUserSSHMessage(msg)
                    elif "AddSshKey" in msg:
                        return AddSshKeyMessage(msg)
                    elif "ListUsersSSH" in msg:
                        raise NotImplementedError
                    elif "DeleteUserSSH" in msg:
                        return DeleteUserSSHMessage(msg)
                    else:
                        return ErrorMessage(
                            {"reason": "options", "message": "Invalid Option", "code": 404}
                        )
                else:
                    return ErrorMessage(
                        {"reason": "token", "message": "Invalid Token", "code": 404}
                    )
            else:
                return ErrorMessage(
                    {"reason": "option", "message": "Token not found", "code": 404}
                )
        except Exception as ex:
            print("Error: JSON message cannot be read! {}".format(str(ex.message)))
