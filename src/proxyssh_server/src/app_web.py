#!/usr/bin/python
# coding: utf-8
# ---------------------------------------------------------------------------------
# O2CMF
# Copyright (C) 2016-2019  Rafael Silva Guimarães
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---------------------------------------------------------------------------------
import os.path
from flask import Flask, Response, request
from flask import render_template_string, jsonify
from werkzeug.contrib.fixers import ProxyFix
from messages import FactoryMessage
import response as resp

app = Flask(__name__)
app.config.from_object(__name__)


def root_dir():
    return os.path.abspath(os.path.dirname(__file__))


def get_file(filename):
    try:
        src = os.path.join(root_dir(), filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        return open(src).read()
    except IOError as exc:
        return str(exc)


@app.route('/')
def index():
    return render_template_string(resp.ROOTPAGE)


@app.route('/static_files/<path:path>')
def get_static(path):  # pragma: no cover
    complete_path = os.path.join(root_dir(), path)
    content = get_file(complete_path)
    return Response(content, mimetype="image/png")


@app.route('/add/user', methods=['GET', 'POST'])
@app.route('/add/sshpubkey', methods=['GET', 'POST'])
def get_resource():
    if request.method == 'POST':
        content = request.json
        if content:
            handle_msg = FactoryMessage.handle(content)
            result = handle_msg.execute()
            return jsonify(result)
    elif request.method == 'GET':
        pass
    else:
        pass

app.wsgi_app = ProxyFix(app.wsgi_app)

if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=8080
    )
