# O2CMF

Testbed control framework developed by UFES to enable converged experimentation (cloud and networking) on federated research infrastructure.

## Install and Run

For instructions regarding O2CMF installation, please take a look at the [**Installation**](https://gitlab.com/futebol/O2CMF/wikis/installation) page on the wiki.

The [wiki](https://gitlab.com/futebol/O2CMF/wikis/home) also offers further information regarding testing and extension of O2CMF.

## Authors

* **Isabella de Albuquerque Ceravolo**
* **Diego Cardoso**
* **Cristina Dominicini**
* **Rafael Guimarães**

## Acknowledgments

This work is part of the FUTEBOL project, which has received funding from the European Union’s Horizon 2020 for research, technological development, and demonstration under grant agreement no. 688941 (FUTEBOL), as well from the Brazilian Ministry of Science, Technology and Innovation (MCTI) through RNP and CTIC. We thank Pedro Alvarez from Trinity College Dublin for information on development of testbed control framework.

## Branches
* `master` &rarr; manages ONLY VMs;
* `nfv_prototype` &rarr; manages VMs and NFV;
* `nfv_ofc` &rarr; NFV features (archived).
